# three-floating-controls

Three.js camera controls.  
Demo 1: https://projects.thibautfoussard.com/lightbulbs/  
Demo 2: https://projects.thibautfoussard.com/dark_slime/

## Install

```bash
npm i three-floating-controls
```

## Usage
```javascript
import FloatingControls from 'three-floating-controls'

let controls = new FloatingControls(camera);

function loop() {
    requestAnimationFrame(loop);
    controls.update();
}
```

## Options

### cameraCenter (Vector3)
Initial position of the camera (the camera will move around this point).  
Useless if you already have defined the camera position beforehand.  
Default is the camera position.  
Example:
```javascript  
let controls = new FloatingControls(camera, {
    cameraCenter: new THREE.Vector3(0, 1, 0)
});
// is equivalent to
camera.position.set(0, 1, 0);
let controls = new FloatingControls(camera);
```

### lookAt (Vector3)
Rotates the camera as it moves to look at the position provided.  
If set to false, the camera will keep its initial rotation.
Default: [0, 0, 0]  
Example:  
```javascript  
let controls = new FloatingControls(camera, {
    lookAt: new THREE.Vector3(0, 1, 0)
});
```

### axisFactor (array)
Influence factor of the controls on the x and y axes.  
Default: [1, 1]
```javascript  
let controls = new FloatingControls(camera, {
    axisFactor: [2, 1] // camera will rotate twice as much on the x axis 
});
```
### amp (float)
Motion amplification. A greater value will make the camera go further.  
Default: 1  
Example:
```javascript  
let controls = new FloatingControls(camera, {
    amp: .3
});
```

### lerpAmount (float, range is [0,1])
For each iteration of the linear interpolation, the amount to interpolate between the mouse position and the camera position. A smaller value will make the animation smoother.    
Default: .1  
Example:
```javascript  
let controls = new FloatingControls(camera, {
    lerpAmount: .05
});
```

### absoluteMinX, absoluteMaxX, absoluteMinY, absoluteMaxY
Absolute min & max coordinates of the camera.  
Default: null  
Example:  
```javascript
let controls = new FloatingControls(camera, {
    absoluteMinX: -10, 
    absoluteMaxX: 10, 
    absoluteMinY: -10, 
    absoluteMaxY: 10
});
```

## Attributes

### active

If set to false, the class will not update the camera position when the mouse position changes.  
Default is true.
```javascript
let controls = new FloatingControls(camera, { ... });
controls.active = false;
```