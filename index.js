import { Vector3, MathUtils } from 'three';

export default class FloatingControls {    
    constructor(camera, opt) {
        let options = opt != undefined ? opt : {};
        this.camera = camera;        
        this.cameraCenter = options.cameraCenter != undefined ? options.cameraCenter : this.camera.position.clone();
        this.windowInnerWidth = null;
        this.windowInnerHeight = null;
        this.amp = typeof options.amp == 'number' ? options.amp : 1;
        this.axisFactor = options.axisFactor || [1, 1];
        this.absoluteMinX = options.absoluteMinX != undefined ? options.absoluteMinX : null;
        this.absoluteMaxX = options.absoluteMaxX != undefined ? options.absoluteMaxX : null;
        this.absoluteMinY = options.absoluteMinY != undefined ? options.absoluteMinY : null;
        this.absoluteMaxY = options.absoluteMaxY != undefined ? options.absoluteMaxY : null;
        this.lerpAmount = options.lerpAmount != undefined ? options.lerpAmount : .1;
        this.lookAt = options.lookAt != undefined ? options.lookAt : new Vector3();
        this.active = true;
        this.getScreenSize();
        window.addEventListener('resize', this.getScreenSize.bind(this));

        this.mousePosition = {x: 0, y: 0};
        window.addEventListener('mousemove', this.updateMousePosition.bind(this));
    }

    getScreenSize() {
        this.windowInnerWidth = window.innerWidth;
        this.windowInnerHeight = window.innerHeight;
    }

    normalizeMousePosition(ev) {
        let normalizedX, normalizedY;
    
        // Desktop
        if (ev.clientX != undefined) {
            normalizedX = -2 * (.5 - ev.clientX / this.windowInnerWidth);
            normalizedY = 2 * (.5 - ev.clientY / this.windowInnerHeight);
        }
        // Mobile 
        else {
            normalizedX = -2 * (.5 - ev.touches[0].clientX / this.windowInnerWidth);
            normalizedY = 2 * (.5 - ev.touches[0].clientY / this.windowInnerHeight);
        }    
        
        return {
            x: normalizedX,
            y: normalizedY
        }
    }
    
    updateMousePosition(ev) {
        this.mousePosition = this.normalizeMousePosition(ev);
    }

    update() {
        if ( !this.active ) {
            return;
        }
        
        let newX = MathUtils.lerp (this.camera.position.x, this.cameraCenter.x + this.mousePosition.x * this.amp * this.axisFactor[0], this.lerpAmount);
        let newY = MathUtils.lerp (this.camera.position.y, this.cameraCenter.y + this.mousePosition.y * this.amp * this.axisFactor[1], this.lerpAmount);

        if (this.absoluteMinX != null) newX = Math.max(this.absoluteMinX, newX);
        if (this.absoluteMaxX != null) newX = Math.min(this.absoluteMaxX, newX);
        if (this.absoluteMinY != null) newY = Math.max(this.absoluteMinY, newY);
        if (this.absoluteMaxY != null) newY = Math.min(this.absoluteMaxY, newY);
        
        this.camera.position.x = newX;
        this.camera.position.y = newY;
        this.camera.position.z = this.cameraCenter.z;

        if ( this.lookAt ) {
            this.camera.lookAt( this.lookAt );
        }
    }
}